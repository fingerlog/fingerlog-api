<?php

return [

    'checkaccess' => [
        'validation_rules' => [
        ]
    ],

    'sign_up' => [
        'release_token' => env('SIGN_UP_RELEASE_TOKEN'),
        'validation_rules' => [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]
    ],

    'giveaccess' => [
        'validation_rules' => [
            'messageid' => 'required',
            'deviceid' => 'required'
        ]
    ],

    'login' => [
        'validation_rules' => [
            'email' => 'required|email',
            'password' => 'required'
        ]
    ],

    'forgot_password' => [
        'validation_rules' => [
            'email' => 'required|email'
        ]
    ],

    'notify' => [
        'validation_rules' => [
            'fingerlog-email' => 'required|email'
        ]
    ],

    'reset_password' => [
        'release_token' => env('PASSWORD_RESET_RELEASE_TOKEN', false),
        'validation_rules' => [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]
    ],

    'check' => [
        'validation_rules' => [
            'deviceid' => 'required'
        ]
    ]

];
