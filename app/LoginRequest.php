<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginRequest extends Model
{
    protected $table = 'loginrequest';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'messageid', 'deviceid', 'pushtoken', 'logged_in',
    ];
}

