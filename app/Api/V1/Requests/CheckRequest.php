<?php

namespace App\Api\V1\Requests;

use Illuminate\Support\Facades\Config;
use Dingo\Api\Http\FormRequest;

class CheckRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.check.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
