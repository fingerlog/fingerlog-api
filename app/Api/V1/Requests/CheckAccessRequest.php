<?php

namespace App\Api\V1\Requests;

use Illuminate\Support\Facades\Config;
use Dingo\Api\Http\FormRequest;

class CheckAccessRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.checkaccess.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
