<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class NotifyRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.notify.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
