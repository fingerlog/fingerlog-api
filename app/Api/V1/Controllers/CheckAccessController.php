<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\CheckAccessRequest;
use App\Http\Controllers\Controller;
use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CheckAccessController extends Controller
{
    public function checkAccess(CheckAccessRequest $request)
    {
        $email = $request->input('fingerlogemail');
        $User = DB::table('users')->where('email', $email)->first();

        if($email) {
            $LoginRequest = DB::table('loginrequest')->where([
                ['deviceid', '=', $User->deviceid]
            ])->orderBy('id','desc')->first();

            if ($LoginRequest->logged_in == 1) {
                    $authenticated = false;
                    $startTime = explode(" ", $LoginRequest->created_at);
                    $date = str_replace('-', '/', $startTime[0]);
                    $time = $startTime[1];

                    $first = Carbon::createFromTimestamp(strtotime($date . " " . $time));
                    $maxLoginTime = $first->copy()->addMinutes(1);
                    if (Carbon::now()->between($first, $maxLoginTime)) {
                        $authenticated = true;
                    }

                    if ($authenticated) {
                        return response()
                            ->json(['status' => 'finished', 'message' => 'Authentication successful'])
                            ->header('Content-Type', "application/json")->header('Access-Control-Allow-Origin', "*");
                    } else {
                        return response()
                            ->json(['status' => 'failed', 'message' => 'Authentication failed.'])
                            ->header('Content-Type', "application/json")->header('Access-Control-Allow-Origin', "*");
                    }
            }
        }

        return response()
            ->json(['status' => 'notfound', 'message' => 'Login session not found.'])
            ->header('Content-Type', "application/json")->header('Access-Control-Allow-Origin', "*");
    }
}
