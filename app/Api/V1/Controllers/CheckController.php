<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Api\V1\Requests\CheckRequest;
use App;
use Log;
use Illuminate\Support\Facades\DB;

class CheckController extends Controller
{

    public function deviceExists(CheckRequest $request)
    {
        $deviceid = $request->input('deviceid');

        if($deviceid) {
            $user = DB::table('users')->where('deviceid', $deviceid)->first();

            if($user) {
                return response()
                    ->json([
                        'status' => 'ok',
                        'deviceid' => $deviceid,
                        'message' => 'deviceid found'
                    ], 200)->header('Access-Control-Allow-Origin', '*');
            } else {
                return response()
                    ->json([
                        'status' => 'error',
                        'deviceid' => $deviceid,
                        'message' => 'deviceid not found'
                    ], 200)->header('Access-Control-Allow-Origin', '*');
            }
        } else {
            return response()
                ->json([
                    'status' => 'error',
                    'error' => '403 - Missing token or device id'
                ], 403)->header('Access-Control-Allow-Origin', '*');
        }
    }

    public function getEmail(CheckRequest $request)
    {
        $deviceID =  $request->input('deviceid');
        Log::info("Get Email request for ",$request->all());

        $user = DB::table('users')->where('deviceid', $deviceID)->first();

        if($user) {
            $useremail = $user->email;
            $username = $user->name;

            return response()
                ->json([
                    'status' => 'ok',
                    'email' => $useremail,
                    'username' => $username,
                    'message' => 'deviceid found'
                ], 200)->header('Access-Control-Allow-Origin', '*');
        } else {
            return response()
                ->json([
                    'status' => 'error',
                    'deviceid' => $deviceID,
                    'message' => 'deviceid not found'
                ], 200)->header('Access-Control-Allow-Origin', '*');
        }
    }
}