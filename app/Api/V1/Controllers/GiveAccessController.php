<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\GiveAccessRequest;
use App\Http\Controllers\Controller;
use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GiveAccessController extends Controller
{
    public function updateAccess(GiveAccessRequest $request)
    {
        $messageid = $request->input('messageid');
        $deviceid = $request->input('deviceid');
        $authenticated = false;

        Log::info("Login for " . $messageid);
        Log::info("Request: " . json_encode($request->all()));

        if($messageid && $deviceid) {
            $loginRequest = DB::table('loginrequest')->where([
                ['messageid', '=', $messageid],
                ['deviceid', '=', $deviceid]
            ])->get();

            Log::info("Login request (for messageid: " . $messageid . "): " . $loginRequest);

            if (isset($loginRequest[0])) {
                $startTime = explode(" ", $loginRequest[0]->created_at);
                $date = str_replace('-', '/', $startTime[0]);
                $time = $startTime[1];

                $first = Carbon::createFromTimestamp(strtotime($date . " " . $time));
                $maxLoginTime = $first->copy()->addMinutes(1);
                if (Carbon::now()->between($first, $maxLoginTime)) {
                    $authenticated = true;
                }

                if ($authenticated) {
                    $update = DB::table('loginrequest')
                        ->where([
                            ['messageid', '=', $messageid],
                            ['deviceid', '=', $deviceid]
                        ])->update(['logged_in' => true]);
                    Log::info('Authentication successful ' . $update);
                    return response()
                        ->json(['status' => 'finished', 'message' => 'Authentication successful'])
                        ->header('Content-Type', "application/json");
                } else {
                    Log::error('Authentication failed due to time: ' . $first . ' and ' . $maxLoginTime . ' are not between ' . Carbon::now());
                    return response()
                        ->json(['status' => 'failed', 'message' => 'Authentication failed.'])
                        ->header('Content-Type', "application/json");
                }
            }
        }

        Log::error('Authentication failed - messageid: ' . $messageid . ' | deviceid: ' . $deviceid);
        return response()
            ->json(['status' => 'failed', 'message' => 'Authentication failed.'])
            ->header('Content-Type', "application/json");
    }
}