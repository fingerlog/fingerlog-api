<?php

namespace App\Api\V1\Controllers;

use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use App;
use Sly;

class LoginController extends Controller
{

    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        $userdata = array(
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );

        try {
            $token = $JWTAuth->attempt($userdata);

            if (!$token) {
                return response()
                    ->json([
                        'status' => 'error',
                        'error' => 'AccessDenied'//new AccessDeniedHttpException()
                    ], 401)->header('Access-Control-Allow-Origin', '*');
            }

        } catch (JWTException $e) {
            return response()
                ->json([
                    'status' => 'error',
                    'error' => '500'//new HttpException(500)
                ], 220)->header('Access-Control-Allow-Origin', '*');
        }

        return response()
            ->json([
                'status' => 'ok',
                'token' => $token
            ], 200)->header('Access-Control-Allow-Origin', '*');
    }
}