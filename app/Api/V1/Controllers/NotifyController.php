<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\NotifyRequest;
use App\Http\Controllers\Controller;
use App;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NotifyController extends Controller
{
    public function notifyUser(NotifyRequest $request)
    {
        $email = $request->input('fingerlog-email');

        if(isset($email)) {

            $User = DB::table('users')->latest()->where([
                ['email', '=', $email]
            ])->get();

            if($User[0]->pushtoken) {
                $this->sendLoginNotification($User[0]->pushtoken);
            } else {
                abort(500, 'User does not have PUSH TOKEN assigned.');
            }
        }
    }

    private function sendLoginNotification($token)
    {
        $messageid = md5(microtime());
        Log::info('Sending notification to ' . $token . ' with messageid: ' . $messageid);
        $msg = array
        (
            'message'               => 'There has been a new login request for your Fingerlog account. Please confirm your identity.',
            'title'                 => 'Fingerlog',
            'subtitle'              => 'Login Request',
            'tickerText'            => 'Ticker',
            'vibrate'               => 1,
            'sound'                 => 1,
            'largeIcon'             => 'large_icon',
            'smallIcon'             => 'small_icon',
            'data'                  => array(
                'messageid'         => $messageid
            ),
            'additionalData'        => array(
                'messageid'         => $messageid
            ),
            'messageid'             => $messageid

        );
        $fields = array
        (
            'to'            => $token,
            'data'          => $msg
        );

        $headers = array
        (
            'Authorization: key=' . env('API_ACCESS_KEY'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = json_decode(curl_exec($ch ));
        curl_close( $ch );

        $User = DB::table('users')->where('pushtoken', $token)->first();

        DB::table('loginrequest')->insert(
            [
                'messageid'     => $messageid,
                'deviceid'      => $User->deviceid,
                'logged_in'     => false,
                'created_at'    => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at'    => \Carbon\Carbon::now()->toDateTimeString()
            ]
        );

        Log::info("Adding new login request to the database -> " . $messageid);

        Log::info(json_encode($result));

        var_dump($result);
    }
}
