<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\IonicPushNotifications\IonicPushChannel;
use NotificationChannels\IonicPushNotifications\IonicPushMessage;

class LoginNotification extends Notification
{
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [IonicPushChannel::class];
    }

    public function toIonicPush($notifiable)
    {
        return IonicPushMessage::create('fingerlogprofile')
            ->title('Fingerlog Authentication Request')
            ->message('Confirm your authentication')
            ->sound('success')
            ->payload(['foo' => 'bar']);
    }
}
