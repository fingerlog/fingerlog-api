<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth',  'middleware' => 'cors'], function(Router $api) {

        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');
        $api->post('getdevice', 'App\\Api\\V1\\Controllers\\CheckController@deviceExists');
        $api->post('notify', 'App\\Api\\V1\\Controllers\\NotifyController@notifyUser');
        $api->post('updateaccess', 'App\\Api\\V1\\Controllers\\GiveAccessController@updateAccess');
        $api->post('checkaccess', 'App\\Api\\V1\\Controllers\\CheckAccessController@checkAccess');
        $api->post('getemail', 'App\\Api\\V1\\Controllers\\CheckController@getEmail');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
    });

    $api->group(['middleware' => 'jwt.auth','cors'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to this item is only for authenticated user. Provide a token in your request!'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh','cors',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
